package com.idiotsbits.test.utilities.mttfi;

import com.idiotsbits.utilities.mttfi.TextFile;
import com.idiotsbits.utilities.mttfi.TextFileIterator;

public class TextFileIteratorRunnable implements Runnable {

	private final TextFileIterator tfIterator;
	private String errorEmptyFile;
	private int totalIterations;

	public TextFileIteratorRunnable(TextFileIterator tfIterator) {
		this.tfIterator = tfIterator;
	}

	@Override
	public void run() {
		int size = 0;
		while (tfIterator.hasNext()) {
			TextFile tf = tfIterator.next();
			if (tf.getText() == null) {
				errorEmptyFile = tf.getAbsolutePath();
				break;
			}
			size++;
		}
		totalIterations = size;
	}

	public String getErrorEmptyFile() {
		return errorEmptyFile;
	}

	public int getTotalIterations() {
		return totalIterations;
	}

}
