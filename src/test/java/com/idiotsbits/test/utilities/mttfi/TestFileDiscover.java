package com.idiotsbits.test.utilities.mttfi;

import com.google.common.base.Predicate;
import com.idiotsbits.utilities.mttfi.FileDiscover;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestFileDiscover {

	@Test
	public void testIsDirectory() {
		Assert.assertTrue(FileDiscover.isDirectory("."), "The current working directory should return true for is" +
				"Directory().");
	}

	@Test
	public void testIsNotDirectory() {
		Assert.assertFalse(FileDiscover.isDirectory("./excercise_ids.txt"));
	}

	@Test
	public void testIsTextFile() {
		Assert.assertTrue(FileDiscover.isTextFile("excercise_ids.txt"), "File should return true for isTextFile()");
	}

	@Test
	public void testIsNotTextFile() {
		Assert.assertFalse(FileDiscover.isTextFile("notTextFile.odd"), "File should return false for isTextFile()");
	}

	@Test
	public void testFilterObjArrayStrings() {
		String[] strings = new String[] { "first.txt", "second.txt", "not.other", "not.odd", "third.json"};
		List<String> filteredStrings = FileDiscover.filterArray(strings, new Predicate<String>() {

			@Override
			public boolean apply(String s) {
				return s.endsWith(".txt") || s.endsWith(".json");
			}
		});

		Assert.assertEquals(filteredStrings.size(), 3);
		Assert.assertEquals(filteredStrings.get(0), "first.txt");
		Assert.assertEquals(filteredStrings.get(1), "second.txt");
		Assert.assertEquals(filteredStrings.get(2), "third.json");
	}

	@Test
	public void testFilterTextFiles() {
		String[] strings = new String[] { "first.txt", "second.txt", "not.other", "not.odd", "third.json"};
		List<String> filteredFiles = FileDiscover.filterFiles(strings, FileDiscover.FileTypes.TEXT);

		Assert.assertNotNull(filteredFiles);
		Assert.assertEquals(filteredFiles.size(), 3);
		Assert.assertEquals(filteredFiles.get(0), "first.txt");
		Assert.assertEquals(filteredFiles.get(1), "second.txt");
		Assert.assertEquals(filteredFiles.get(2), "third.json");
	}

	@Test
	public void testFilterDirs() {
		String[] strings = new String[] { "first.txt", "second.txt", ".", "not.odd", "third.json"};
		List<String> filteredFiles = FileDiscover.filterDirs(strings);

		Assert.assertEquals(filteredFiles.size(), 1);
		Assert.assertEquals(filteredFiles.get(0), ".");
	}

	@Test
	public void testListFilesInJar() {
		List<String> files = null;
		try {
			files = FileDiscover.discoverFiles(new File("./src/test/resources/item-activities_0.jar"),
			                                                FileDiscover.FileTypes.TEXT, false);
		} catch (IOException e) {
			Assert.fail(e.getMessage(), e);
		}

		Assert.assertNotNull(files);
		Assert.assertTrue(files.size() > 0);
	}

	@Test
	public void testListFilesInDir() {
		List<String> files = null;
		try {
			files = FileDiscover.discoverFiles(new File("./src/test/resources/specific-size-dir"), FileDiscover.FileTypes.TEXT,
			                                              false);
		} catch (IOException e) {
			Assert.fail(e.getMessage(), e);
		}

		Assert.assertNotNull(files);
		Assert.assertEquals(files.size(), 50);
	}

	@Test
	public void testListFilesInSubDir() {
		List<String> files = null;
		try {
			files = FileDiscover.discoverFiles(new File("./src/test/resources/specific-size-dir"), FileDiscover.FileTypes.TEXT,
			                                   true);
		} catch (IOException e) {
			Assert.fail(e.getMessage(), e);
		}

		Assert.assertNotNull(files);
		Assert.assertEquals(files.size(), 65);
	}

}
