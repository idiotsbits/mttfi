package com.idiotsbits.test.utilities.mttfi;

import com.idiotsbits.utilities.mttfi.TextFile;
import com.idiotsbits.utilities.mttfi.TextFileIterator;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestTextFileIterator {

	@Test
	public void testTextFileIteratorDirectory1Thread() throws IOException {
		Iterator<TextFile> iterator = new TextFileIterator(new File("./src/test/resources/specific-size-dir"), true, 0, 1);

		int size = 0;
		while (iterator.hasNext()) {
			Assert.assertNotNull(iterator.next().getText());
			size++;
		}

		Assert.assertEquals(size, 65);
	}

	@Test
	public void testTextFileIteratorDirectory5Threads() throws IOException {
		List<Thread> threads = new ArrayList<>();
		List<TextFileIteratorRunnable> runnables = new ArrayList<>();
		int totalThreads = 5;
		int minimumIterations = 65 / totalThreads;

		for (int i = 0; i < totalThreads; i++) {
			TextFileIteratorRunnable runnable = new TextFileIteratorRunnable(
					new TextFileIterator(new File("./src/test/resources/specific-size-dir"), true, i, totalThreads));
			runnables.add(runnable);
			Thread thread = new Thread(runnable);
			threads.add(thread);
			thread.start();
		}

		boolean wait;
		do {
			wait = false;
			for (Thread thread : threads) {
				if (thread.isAlive()) {
					wait = true;
					break;
				}
			}
		} while (wait);

		int totalIterations = 0;
		for (TextFileIteratorRunnable runnable : runnables) {
			Assert.assertNull(runnable.getErrorEmptyFile());
			Assert.assertTrue(minimumIterations <= runnable.getTotalIterations() && runnable.getTotalIterations() <= (minimumIterations + 1));
			totalIterations += runnable.getTotalIterations();
		}

		Assert.assertEquals(totalIterations, 65);
	}

	@Test
	public void testTextFileIteratorJar1Thread() throws IOException {
		Iterator<TextFile> iterator = new TextFileIterator(new File("./src/test/resources/item-activities_0.jar"), true, 0, 1);

		int size = 0;
		while (iterator.hasNext()) {
			Assert.assertNotNull(iterator.next().getText());
			size++;
		}

		Assert.assertEquals(size, 65);
	}

	@Test
	public void testTextFileIteratorJar5Threads() throws IOException {
		List<Thread> threads = new ArrayList<>();
		List<TextFileIteratorRunnable> runnables = new ArrayList<>();
		int totalThreads = 5;
		int minimumIterations = 65 / totalThreads;

		for (int i = 0; i < totalThreads; i++) {
			TextFileIteratorRunnable runnable = new TextFileIteratorRunnable(
					new TextFileIterator(new File("./src/test/resources/item-activities_0.jar"), true, i, totalThreads));
			runnables.add(runnable);
			Thread thread = new Thread(runnable);
			threads.add(thread);
			thread.start();
		}

		boolean wait;
		do {
			wait = false;
			for (Thread thread : threads) {
				if (thread.isAlive()) {
					wait = true;
					break;
				}
			}
		} while (wait);

		int totalIterations = 0;
		for (TextFileIteratorRunnable runnable : runnables) {
			Assert.assertNull(runnable.getErrorEmptyFile());
			Assert.assertTrue(minimumIterations <= runnable.getTotalIterations() && runnable.getTotalIterations() <= (minimumIterations + 1));
			totalIterations += runnable.getTotalIterations();
		}

		Assert.assertEquals(totalIterations, 65);
	}

}
