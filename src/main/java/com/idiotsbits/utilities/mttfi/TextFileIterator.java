package com.idiotsbits.utilities.mttfi;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * <p><b>TextFileIterator</b>
 * <p>The text file iterator builds a list of text files from a source directory or jar file. Then,
 * creates a buffer of files with the contents of the files pre-loaded for consumption. As the iterator progresses
 * through the list, files are fed into the buffer for content loading.
 *
 * <p>The intention of this iterator is to provide a low memory usage solution for reading the contents of numerous
 * text files in a sequential order (which is currently only as they are discovered).
 *
 * <p>The TextFileIterator accounts for a multithreaded iteration. Multiple threads can iterate over the same source
 * directory or jar file with each file only being read by one thread. This is accomplished by dividing the total
 * number of files by the total number of threads. There is no control over how fast a thread/iterator traverses over
 * its assigned fileset.
 */
public class TextFileIterator implements Iterator<TextFile> {

	private List<String> allFiles;
	private Queue<TextFile> fileBuffer;
	private int maxQueueSize = 7;
	private int currentIndex;
	private int nextIndex;
	private int queueItemIndex;
	private int queueNextItemIndex;
	private final int totalThreads;
	private final int threadNum;
	private final Logger LOG = LoggerFactory.getLogger(TextFileIterator.class);

	/**
	 * TextFileIterator
	 * @param directoryOrJar as the root directory or jar within which to find the text files.
	 * @param recursive if true and a directory, then subdirectories will also be searched.
	 * @param threadNum as the thread number for this iterator. <b>0 based index</b>
	 * @param totalThreads as the total number of threads acting on this directory or jar.
	 * @throws IOException if failure during file discovery.
	 */
	public TextFileIterator(File directoryOrJar, boolean recursive, int threadNum, int totalThreads) throws IOException {
		Preconditions.checkArgument(directoryOrJar != null && directoryOrJar.exists(), "Directory or Jar must not be null", directoryOrJar);
		Preconditions.checkArgument(directoryOrJar.isDirectory() || FileDiscover.isJar(directoryOrJar),
		                            "File must be a directory or a Jar");
		Preconditions.checkArgument(threadNum < totalThreads, "The parameter threadNum is a 0 based index, therefore " +
				"the largest thread number will be totalThreads - 1.");

		this.totalThreads = totalThreads;
		this.threadNum = threadNum;
		// discover files and load queue
		this.allFiles = FileDiscover.discoverFiles(directoryOrJar, FileDiscover.FileTypes.TEXT, recursive);
		if (this.allFiles == null) {
			this.allFiles = new ArrayList<>();
		}
		resetQueue();
	}

	/**
	 * Returns the iterator back to the beginning of the file list and resets the file buffer.
	 */
	public void reset() {
		resetQueue();
	}

	/**
	 * Returns the iterator and queue back to the beginning of the file list.
	 */
	private void resetQueue() {
		this.fileBuffer = new ArrayDeque<>(maxQueueSize);
		this.currentIndex = this.nextIndex = this.queueItemIndex = this.queueNextItemIndex = this.threadNum;
		if (this.allFiles != null && !this.allFiles.isEmpty()) {
			for (int i = 0; i < (maxQueueSize - 1); i++) {
				addNextOnQueue();
			}
		}
	}

	/**
	 * If there are more files in the list, places the next file in the queue.
	 */
	private void addNextOnQueue() {
		if (this.nextIndex < this.allFiles.size()) {
			this.currentIndex = this.nextIndex;
			this.nextIndex += this.totalThreads;
			TextFile next = new TextFile(this.allFiles.get(this.currentIndex));
			next.loadContents();
			this.fileBuffer.add(next);
		}
	}

	/**
	 * If there is more in the queue, removes the head queue object.
	 * @return
	 */
	private TextFile removeFirstOnQueue() {
		if (!hasNext()) {
			return null;
		}

		this.queueItemIndex = this.queueNextItemIndex;
		this.queueNextItemIndex += this.totalThreads;
		return this.fileBuffer.remove();
	}

	/**
	 * Returns true if the queue has more objects.
	 * @return true if the queue has more objects.
	 */
	public boolean hasNext() {
		return !this.fileBuffer.isEmpty();
	}

	/**
	 * Returns the next TextFile from the head of the queue. If the queue is empty, null is returned.
	 * @return the next TextFile from the head of the queue. If the queue is empty, null is returned.
	 */
	public TextFile next() {
		if (!hasNext()) {
			return null;
		}

		addNextOnQueue();
		return removeFirstOnQueue();
	}

	/**
	 * Removes the last viewed item from the collection.
	 */
	public void remove() {
		this.allFiles.remove(this.queueItemIndex);
	}

}
