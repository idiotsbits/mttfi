package com.idiotsbits.utilities.mttfi;

import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * <p><b>ContentLoader</b>
 * <p>Reads the contents of a TextFile
 */
public class ContentLoader implements Runnable {

	private TextFile textFile;
	private Logger logger = LoggerFactory.getLogger(ContentLoader.class);

	public ContentLoader(TextFile textFile) {
		this.textFile = textFile;
	}

	@Override
	public void run() {
		byte[] bytes;
		if (textFile.getAbsolutePath().startsWith("jar")) {
			bytes = readBytesFromJar();
		} else {
			bytes = readBytesFromFile();
		}
		textFile.setContents(bytes);
	}

	private byte[] readBytesFromJar() {
		try {
			URL url = new URL(textFile.getAbsolutePath());
			return Resources.toByteArray(url);
		} catch (IOException e) {
			if (logger.isErrorEnabled()) {
				logger.error("Error while tring to read contents of file within jar.");
				logger.error(e.getMessage(), e);
			}

			return new byte[0];
		}
	}

	private byte[] readBytesFromFile() {
		try {
			return Files.asByteSource(new File(textFile.getAbsolutePath())).read();
		} catch (IOException e) {
			if (logger.isErrorEnabled()) {
				logger.error("Error while tring to read contents of file.");
				logger.error(e.getMessage(), e);
			}

			return new byte[0];
		}
	}

}
