package com.idiotsbits.utilities.mttfi;

/**
 * <p><b>TextFile</b>
 *
 * <p>Represents a text file in the file system.
 */
public class TextFile {

	private final String absolutePath;
	private byte[] contents;
	private boolean readingContents;

	/**
	 * Represents a text file in the file system. If the file exists in a jar file, the absolute path should be a URL.
	 * @param absolutePath as the absolute path to the text file in a directory or jar file.
	 */
	public TextFile(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	/**
	 * Loads the text contents of the file into the class field 'contents'. This method uses it's own thread to perform
	 * the file reading.
	 */
	public void loadContents() {
		if (!readingContents) {
			readingContents = true;
			Thread loader = new Thread(new ContentLoader(this));
			loader.start();
		}
	}

	/**
	 * Returns the text contents of the file.
	 * @return the text contents of the file.
	 */
	public String getText() {
		return new String(getBytes());
	}

	/**
	 * Returns the byte[] contents of the file.
	 * @return the byte[] contents of the file.
	 */
	public byte[] getBytes() {
		if (contents == null) {
			loadContents();
		}
		// wait for contents to be loaded
		while (readingContents) {
			try {
				Thread.sleep(5);
			} catch (InterruptedException ignore) {
			}
		}

		return contents;
	}

	/**
	 * Sets the text contents of the file.
	 * @param contents as the file contents.
	 */
	protected void setContents(byte[] contents) {
		this.contents = contents;
		this.readingContents = false;
	}

	/**
	 * Returns the absolute path of the text file.
	 * @return the absolute path of the text file.
	 */
	public final String getAbsolutePath() {
		return absolutePath;
	}

}
