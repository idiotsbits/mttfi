package com.idiotsbits.utilities.mttfi;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * <p>
 * <b>FileDiscover</b>
 * </p>
 * <p>
 * Utility methods for finding files in a directory or zip file.
 * </p>
 */
public class FileDiscover {

	/**
	 * Types of files that FileDiscover is capable of filtering.
	 */
	public enum FileTypes {
		/**
		 * .json, .txt, .text, .html, .htm, .xml
		 */
		TEXT,
		/**
		 * .jar
		 */
		JAR
	}

	/**
	 * Returns true if the file ends with the '.jar' extension.
	 * @param file as the file to check
	 * @return true if the file ends with the '.jar' extension.
	 */
	public static boolean isJar(File file) {
		return file != null && file.getAbsolutePath().endsWith(".jar");
	}

	/**
	 * Finds files of the specified type within the provided directory or zip file. If recursive is set to true and
	 * the specified file is a directory, subdirectories will also be searched.
	 * @param dirOrZip as the directory or zip file within which to search.
	 * @param type as the type of files to find/discover.
	 * @param recursive if true and the dirOrZip is a directory, then subdirectories will also be searched.
	 * @return a List file names that match the type specified or an empty list if no files match.
	 * @throws IOException if a file does not exist.
	 */
	public static List<String> discoverFiles(File dirOrZip, FileTypes type, boolean recursive) throws IOException {
		Preconditions.checkArgument(dirOrZip != null && dirOrZip.exists(),
		                            "The provided dir must not be null and must exist.");
		if (dirOrZip.isDirectory()) {
			return discoverFilesInDir(dirOrZip, type, recursive);
		} else if (isJar(dirOrZip)) {
			return discoverFilesInJar(dirOrZip, type);
		} else {
			Preconditions.checkArgument(false,
			                            "The provided file is neither a directory or zip file. No files can be discovered.");
			return null;
		}
	}

	/**
	 * Finds files of the specified type within the provided directory. If recursive is set to true, subdirectories will
	 * also be searched.
	 * @param dir as the directory within which to search.
	 * @param type as the type of files to find/discover.
	 * @param recursive if true, then subdirectories will also be searched.
	 * @return a List file names that match the type specified or an empty list if no files match.
	 */
	protected static List<String> discoverFilesInDir(File dir, FileTypes type, boolean recursive) {
		String[] dirList = dir.list();
		if (dirList == null) {
			return null;
		} else {
			List<String> finalList = new ArrayList<>();

			// add top level files that match the type
			List<String> typeMatchFiles = filterFiles(dirList, type);
			if (typeMatchFiles != null) {
				List<String> fullPathFiles = prependPathTo(typeMatchFiles, dir.getAbsolutePath());
				finalList.addAll(fullPathFiles);
			}

			// search subdirectories for files that match
			if (recursive) {
				List<String> subDirs = filterDirs(dirList, dir.getAbsolutePath());
				if (subDirs != null) {
					for (String subDir : subDirs) {
						List<String> subDirFiles = discoverFilesInDir(new File(subDir), type, true);
						if (subDirFiles != null) {
							finalList.addAll(subDirFiles);
						}
					}
				}
			}
			return finalList;
		}
	}

	/**
	 * Finds files of the specified type within the provided zip file.
	 * @param sourceJar as the zip file within which to search.
	 * @param type as the type of files to find/discover.
	 * @return a List file names that match the type specified or an empty list if no files match.
	 * @throws IOException if a file does not exist.
	 */
	protected static List<String> discoverFilesInJar(File sourceJar, FileTypes type) throws
	                                                                                 IOException {
		List<String> allFiles = listFilesInZip(sourceJar);
		List<String> finalList = new ArrayList<>();

		List<String> typeMatchFiles = filterFiles(allFiles, type);
		if (typeMatchFiles != null) {
			List<String> fullPathFiles = prependPathTo(typeMatchFiles,
			                                           String.format("jar:file:%s!/", sourceJar.getAbsolutePath()));
			finalList.addAll(fullPathFiles);
		}

		return finalList;
	}

	/**
	 * Lists all files within a zip file.
	 * @param sourceZip as the source zip file.
	 * @return a List of all filenames within the zip file.
	 * @throws IOException if a zip format or I/O exception occurs.
	 */
	public static List<String> listFilesInZip(File sourceZip) throws IOException {
		ZipFile zipFile = new ZipFile(sourceZip);
		List<String> filesInZip = null;

		Enumeration<? extends ZipEntry> zipFileEntries = zipFile.entries();
		if (zipFileEntries != null) {
			filesInZip = new ArrayList<>();
			while (zipFileEntries.hasMoreElements()) {
				ZipEntry entry = zipFileEntries.nextElement();
				filesInZip.add(entry.getName());
			}
		}

		return filesInZip;
	}

	/**
	 * Filters the provided list of files based on the provided FileType.
	 * @param files as the original list of files.
	 * @param type as the FileType to filter by.
	 * @return a list of files that match the provided FileType or an empty list if no matching files are found.
	 */
	public static List<String> filterFiles(List<String> files, FileTypes type) {
		if (files != null && files.size() > 0) {
			String[] filesArray = new String[files.size()];
			return filterFiles(files.toArray(filesArray), type);
		} else {
			return null;
		}
	}

	/**
	 * Filters the provided list of files based on the provided FileType.
	 * @param files as the original list of files.
	 * @param type as the FileType to filter by.
	 * @return a list of files that match the provided FileType or an empty list if no matching files are found.
	 */
	public static List<String> filterFiles(String[] files, FileTypes type) {
		switch (type) {
			case TEXT:
				return filterTextFiles(files);
			default:
				return null;
		}
	}

	/**
	 * Returns a list of directories from the original list of files. If no directories are found, null is returned.
	 * The rootPath is prepended to all of the files before the directory search.
	 * @param files as the original list of files.
	 * @param rootPath as the root path to all of the files (assumes they all have the same root path).
	 * @return a list of directories from the original list of files or an empty list if no directories are found.
	 */
	public static List<String> filterDirs(String[] files, String rootPath) {
		String[] fullPathFiles = prependPathTo(files, rootPath);

		if (fullPathFiles != null) {
			return filterDirs(fullPathFiles);
		} else {
			return new ArrayList<>();
		}
	}

	/**
	 * Adds the root path to the begining of each of the strings in the array. Ensures that a single '/' character
	 * sits between the root path and the file string.
	 * @param files as the list of file names or relative paths
	 * @param rootPath as the root path
	 * @return a final array of file names or relative paths prepended with the root path.
	 */
	public static String[] prependPathTo(String[] files, String rootPath) {
		if (files == null) {
			return null;
		}

		if (rootPath == null || rootPath.trim().isEmpty()) {
			return files;
		}

		String knownRootPath = (!rootPath.endsWith("/")) ? rootPath.concat("/"): rootPath;

		String[] fullPathFiles = new String[files.length];
		for (int i = 0; i < files.length; i++) {
			StringBuilder sb = new StringBuilder(knownRootPath);
			if (files[i].startsWith("/")) {
				sb.append(files[i].substring(1));
			} else {
				sb.append(files[i]);
			}
			fullPathFiles[i] = sb.toString();
		}

		return fullPathFiles;
	}

	/**
	 * Adds the root path to the beginning of each of the strings in the array. Ensures that a single '/' character
	 * sits between the root path and the file string.
	 * @param files as the list of file names or relative paths
	 * @param rootPath as the root path
	 * @return a final array of file names or relative paths prepended with the root path.
	 */
	public static List<String> prependPathTo(List<String> files, String rootPath) {
		if (files == null) {
			return null;
		}

		if (rootPath == null || rootPath.trim().isEmpty()) {
			return files;
		}

		String knownRootPath = (!rootPath.endsWith("/")) ? rootPath.concat("/"): rootPath;

		List<String> fullPathFiles = new ArrayList<>();
		for (String file : files) {
			StringBuilder sb = new StringBuilder(knownRootPath);
			if (file.startsWith("/")) {
				sb.append(file.substring(1));
			} else {
				sb.append(file);
			}
			fullPathFiles.add(sb.toString());
		}

		return fullPathFiles;
	}

	/**
	 * Returns a list of directories from the original list of files. If no directories are found, null is returned.
	 * @param files as the original list of files.
	 * @return a list of directories from the original list of files or an empty list if no directories are found.
	 */
	public static List<String> filterDirs(String[] files) {
		return filterArray(files, new Predicate<String>() {

			@Override
			public boolean apply(String s) {
				return isDirectory(s);
			}
		});
	}

	/**
	 * Returns a list of text files from the original list of files. If no text files are found, null is returned.
	 * @param files as the original list of files.
	 * @return a list of text files from the original list of files. If no text files are found, an empty list is
	 * returned.
	 */
	protected static List<String> filterTextFiles(String[] files) {
		return filterArray(files, new Predicate<String>() {

			@Override
			public boolean apply(String s) {
				return isTextFile(s);
			}
		});
	}

	/**
	 * Filters an array of objects based on the predicate. Returns an empty list if no objects return true for the
	 * predicate.
	 * @param originals as the original array of objects.
	 * @param predicate as the predicate to apply.
	 * @param <T> of Object
	 * @return a list of objects for which the predicate returns true or an empty list if none return true.
	 */
	public static <T> List<T> filterArray(T[] originals, Predicate<T> predicate) {
		List<T> finalList = new ArrayList<>();
		if (originals == null) {
			return null;
		}

		for (T orig : originals) {
			if (predicate.apply(orig)) {
				finalList.add(orig);
			}
		}

		return finalList;
	}

	/**
	 * Returns true if the file extension matches a predetermined list of text file extensions.
	 * .json, .txt, .text, .htm, .html, .xml
	 * @param fileName as the file name
	 * @return true if the file extension matches a predetermined list of text file extensions.
	 */
	public static boolean isTextFile(String fileName) {
		if (Strings.isNullOrEmpty(fileName)) {
			return false;
		}
		String lowerCaseName = fileName.toLowerCase();
		return lowerCaseName.endsWith(".json")
				|| lowerCaseName.endsWith(".txt")
				|| lowerCaseName.endsWith(".text")
				|| lowerCaseName.endsWith(".htm")
				|| lowerCaseName.endsWith(".html")
				|| lowerCaseName.endsWith(".xml");
	}

	/**
	 * Returns true if the file is a directory.
	 * @param filePath as the file path of the directory.
	 * @return tue if the file is a directory.
	 */
	public static boolean isDirectory(String filePath) {
		try {
			File file = new File(filePath);
			return file.isDirectory();
		} catch (Exception ignore) {
			return false;
		}
	}

}
