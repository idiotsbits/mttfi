# Multi-Thread Text File Iterator (MTTFI) #

MTTFI is a maven project and can be built with the maven package command. Dependencies are not included, but the only dependency is com.google.guava

## What is MTTFI for? ##

MTTFI was originally developed to perform performance "soak" tests of new system environments. These types of tests require large amounts of data to be 'pumped' into a new system setup over a long period of time. MTTFI allows a performance team to pre-build a large data set of specific valid data and save it as a flat-file database. MTTFI will load and read each file during a performance test, while using little Java Heap space, making this a viable option for performance testing which often requires thousands or millions of files.

### Questions? ###

I appreciate comments and suggestions.
